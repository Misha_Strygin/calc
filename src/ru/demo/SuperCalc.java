package ru.demo;

/**
 * Данный класс является скелетом программы калькулятор и имеет несколько методов
 *
 * @autor Стрыгин М.
 */

public class SuperCalc {
    /**
     * Метод возвращающий сумму двух чисел
     *
     * @param a первое слогаемое
     * @param b второе слогаемое
     * @return число равное a + b
     */
    public static double nAmount(double a, double b) {
        return a + b;
    }

    /**
     * Метод возвращающий разность двух чисел
     *
     * @param a Уменьшаемое
     * @param b Вычитаемое
     * @return число равное a - b
     */
    public static double nDifference(double a, double b) {
        return a - b;
    }

    /**
     * Метод возвращающий произведение двух чисел
     *
     * @param a первый множитель
     * @param b вторый множитель
     * @return число равное a * b
     */
    public static double nMultiplication(double a, double b) {
        return a * b;
    }

    /**
     * Метод возвращающий вещественное деление двух чисел
     *
     * @param a делитель
     * @param b делимое
     * @return число равное a / b
     */
    public static double nDivision(double a, double b) {
        return a / b;
    }

    /**
     * Метод возвращающий целочисленное деление двух чисел
     *
     * @param a делитель
     * @param b делимое
     * @return число равное a / b
     */
    public static int nDivision(int a, int b) {
        return a / b;
    }

    /**
     * Метод возвращающий остаток от деление двух чисел
     *
     * @param a делитель
     * @param b делимое
     * @return число равное остатку от a / b
     */
    public static double nRest(double a, double b) {
        return a % b;
    }

    /**
     * Метод возвращающий число возведенное в степень
     *
     * @param a число
     * @param b степень числа
     * @return число a в степени b
     */
    public static double nDegree(double a, int b) {
        double s = 1;
        if (b == 0) {
            return s;
        }
        if (b == 1) {
            return a;
        }
        for (int i = 0; i < b; i++) {
            s *= a;
        }
        return s;

    }
}
