package ru.demo;

import java.util.Scanner;

/**
 * Класс демонстрирующий работы класса SuperCalc
 *
 * @autor Стрыгин М.
 */


public class Demo {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Напишите ваше выражение так, чтобы знак операчии окружал пробел, пример:5 + 6");
        String expression = scanner.nextLine();
        String[] symbols = expression.split(" ");
        double firstOperand = Double.valueOf(symbols[0]);
        double secondOperand = Double.valueOf(symbols[2]);

        switch (symbols[1]) {
            case "+":
                System.out.println("Судя по знаку вы хотите выполнить сложение чисел, а вот и резельтат " + SuperCalc.nAmount(firstOperand, secondOperand));
                break;

            case "-":
                System.out.println("Ооо, Это неплохо использовать калькулятор для таких сложных операций как вычитание, ну чтож вот ваш результат " + SuperCalc.nDifference(firstOperand, secondOperand));
                break;

            case "*":
                System.out.println("Хмм, Это уже посложней, но я машина и не испытываю трудностей , ваш результат " + SuperCalc.nMultiplication(firstOperand, secondOperand));
                break;

            case "/":
                if (secondOperand == 0) {
                    System.out.println("Уважаемый, а вы знали, что на 0 делить нельзя?");
                }
                if (firstOperand % 1 == 0 || secondOperand % 1 == 0) {
                    System.out.println("Это уже поинтересней, деление значит, вот результат " + SuperCalc.nDivision((int) firstOperand, (int) secondOperand));
                } else {
                    System.out.println("Это уже поинтересней, деление значит, вот результат " + SuperCalc.nDivision(firstOperand, secondOperand));
                }
                break;

            case "%":
                System.out.println("А вот и ваш остаток от деления " + SuperCalc.nRest(firstOperand, secondOperand));
                break;

            case "^":
                System.out.println("Это просто легкотня число в степень, вот результат " + SuperCalc.nDegree(firstOperand, (int) secondOperand));
                break;
            default:
                System.out.println("Я пока не знаю этой операции, напишите автору программы, чтобы он что-то зделал с этим.");
                break;
        }
    }
}
